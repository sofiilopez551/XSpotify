#pragma once

//Base
#define TAGLIB_STATIC
#include <Windows.h>
#include <iostream>
#include <urlmon.h>
#include <tchar.h>
#include <string>
#include <fstream>
#include <WinInet.h>
#include <shellapi.h>
#include <sstream>
#include <thread>
#include <vector>
#include <cassert>
#include <taglib/ogg/vorbis/vorbisfile.h>
#include <taglib/utils.h>
#include <openssl/evp.h>


//Libs
#pragma comment(lib, "wininet.lib")
#pragma comment(lib, "urlmon.lib")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "libcryptoMT.lib")
#pragma comment(lib, "tag.lib")

#include "Loader.hpp"
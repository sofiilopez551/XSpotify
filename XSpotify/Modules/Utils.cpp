#pragma once
#include "..\\include\BaseInclude.hpp"

namespace Utils
{
	void Utils::PressMediaKey(int key)
	{
		INPUT input;
		WORD vkey = key;
		input.type = INPUT_KEYBOARD;
		input.ki.wScan = MapVirtualKey(vkey, MAPVK_VK_TO_VSC);
		input.ki.time = 0;
		input.ki.dwExtraInfo = 0;
		input.ki.wVk = vkey;
		input.ki.dwFlags = 0;
		SendInput(1, &input, sizeof(INPUT));
		input.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &input, sizeof(INPUT));
	}

	void Utils::removeForbiddenChar(std::string* s)
	{
		std::string::iterator it;

		for (it = s->begin(); it < s->end(); ++it) {
			switch (*it)
			{
			case '/':case '\\':case ':':case '?':case '"':case '<':case '>':case '|': case ';':case '+':case '@':case '=':case ',':case '#': case '*':case '~':
				*it = '_';
			}
		}
	}

	std::string Utils::hex2string(std::string str)
	{
		std::string res;
		res.reserve(str.size() / 2);
		for (int i = 0; i < str.size(); i += 2)
		{
			std::istringstream iss(str.substr(i, 2));
			int temp;
			iss >> std::hex >> temp;
			res += static_cast<char>(temp);
		}

		return res;
	}

	std::wstring Utils::ConvertUtf8ToUtf16(const std::string& str)
	{
		std::wstring convertedString;
		int requiredSize = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
		if (requiredSize > 0)
		{
			std::vector<wchar_t> buffer(requiredSize);
			MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], requiredSize);
			convertedString.assign(buffer.begin(), buffer.end() - 1);
		}

		return convertedString;
	}

	void Utils::handleErrors(void)
	{
		abort();
	}

	std::string Utils::b64_encode(BYTE const* buf, unsigned int bufLen)
	{

		static const std::string base64_chars =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz"
			"0123456789+/";
		std::string ret;
		int i = 0;
		int j = 0;
		BYTE char_array_3[3];
		BYTE char_array_4[4];

		while (bufLen--)
		{
			char_array_3[i++] = *(buf++);
			if (i == 3)
			{
				char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
				char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
				char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
				char_array_4[3] = char_array_3[2] & 0x3f;

				for (i = 0; (i < 4); i++)
					ret += base64_chars[char_array_4[i]];
				i = 0;
			}
		}

		if (i)
		{
			for (j = i; j < 3; j++)
				char_array_3[j] = '\0';

			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (j = 0; (j < i + 1); j++)
				ret += base64_chars[char_array_4[j]];

			while ((i++ < 3))
				ret += '=';
		}

		return ret;
	}

	void Utils::deleteFile(const std::wstring& filename)
	{
		DeleteFile(filename.c_str());
	}

	bool Utils::FileExists(std::string file)
	{
		std::ifstream filetocheck(file.c_str());
		return filetocheck.good();
	}

	bool Utils::PathExists(std::string path)
	{
		struct stat buffer;
		return (stat(path.c_str(), &buffer) == 0);
	}

	int Utils::GetFileSize(std::string file)
	{
		std::streampos begin, end;
		std::ifstream myfile(file.c_str(), std::ios::binary);
		begin = myfile.tellg();
		myfile.seekg(0, std::ios::end);
		end = myfile.tellg();
		myfile.close();
		return (end - begin);
	}
}
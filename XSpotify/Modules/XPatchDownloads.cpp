#include "..\\include\BaseInclude.hpp"

namespace Modules
{
	bool Downloads::check = false;
	struct Downloads::DecryptedSong_s Downloads::DecryptedSong;
	struct Downloads::EncryptedSong_s Downloads::EncryptedSong;

	std::string Downloads::HttpRequest(std::string site, std::string param, std::string accessToken, bool isaccessToken)
	{
		std::string authHeader;
		InternetScopedHandle hInternet = InternetOpenW(L"1337", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);

		if (isaccessToken == true)
		{
			authHeader = "Authorization: Bearer " + accessToken;
		}
		else
		{
			authHeader = "1337";
		}

		if (hInternet == NULL)
		{
			return "InternetOpenW failed(hInternet): " + std::to_string(GetLastError());
		}

		InternetScopedHandle hConnect = InternetConnectA(hInternet, site.c_str(), 80, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);

		if (hConnect == NULL)
		{
			return "InternetConnectW failed(hConnect == NULL): " + std::to_string(GetLastError());
		}
		InternetScopedHandle hRequest = HttpOpenRequestA(hConnect, "GET", param.c_str(), NULL, NULL, NULL, INTERNET_FLAG_NO_AUTH, 0);

		if (hRequest == NULL)
		{
			return "HttpOpenRequestW failed(hRequest == NULL): " + std::to_string(GetLastError());
		}
		HttpAddRequestHeadersA(hRequest, authHeader.c_str(), -1, HTTP_ADDREQ_FLAG_ADD | HTTP_ADDREQ_FLAG_REPLACE);

		BOOL bRequestSent = HttpSendRequestA(hRequest, NULL, 0, NULL, 0);

		if (!bRequestSent)
		{
			return "!bRequestSent    HttpSendRequestW failed with error code " + std::to_string(GetLastError());
		}

		std::string strResponse;
		const int nBuffSize = 1024;
		char buff[nBuffSize];

		BOOL bKeepReading = true;
		DWORD dwBytesRead = -1;

		while (bKeepReading && dwBytesRead != 0)
		{
			bKeepReading = InternetReadFile(hRequest, buff, nBuffSize, &dwBytesRead);
			strResponse.append(buff, dwBytesRead);
		}
		return strResponse;
	}

	int Downloads::decrypt(unsigned char* ciphertext, int ciphertext_len, unsigned char* key, unsigned char* iv, unsigned char* plaintext)
	{
		EVP_CIPHER_CTX* ctx;

		int len;

		int plaintext_len;

		if (!(ctx = EVP_CIPHER_CTX_new()))
			Utils::Utils::handleErrors();

		if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key, iv))
			Utils::Utils::handleErrors();


		if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
			Utils::Utils::handleErrors();
		plaintext_len = len;


		if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
			Utils::Utils::handleErrors();
		plaintext_len += len;

		EVP_CIPHER_CTX_free(ctx);

		return plaintext_len;
	}

	void __declspec(naked) Downloads::AES_set_encrypt_key_stub(unsigned int* key, DWORD* userKey, int bits)
	{
		__asm
		{
			push     ebp
			mov      ebp, esp
			mov      edx, [ebp + 8]
			push     106B926h
			retn
		}
	}

	void Downloads::AES_set_encrypt_key_hk(unsigned int* key, DWORD* userKey, int bits)
	{
		unsigned char decrypt_key[16] = { 0 };
		char decrypt_key_str[33] = { 0 };

		if (bits == 128)
		{
			unsigned char _userKey[16] = { 0 };

			for (int i = 0; i < 16; i++)
			{
				_userKey[i] = *(char*)((DWORD)userKey + i);
			}

			if (memcmp(_userKey, decrypt_key, 16) != 0)
			{
				for (int i = 0; i < 16; i++)
				{
					sprintf(decrypt_key_str + i * 2, "%02X", _userKey[i]);
				}

				EncryptedSong.decryptionKey = decrypt_key_str;
			}
		}

		AES_set_encrypt_key_stub(key, userKey, bits);
	}

	std::string Downloads::GetAccessToken()
	{
		ShellExecute(NULL, L"open", L".\\token.exe", NULL, NULL, SW_HIDE);
		Sleep(1000);

		std::ifstream ifs("token.txt");
		std::string accessToken((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		return accessToken;
	}

	std::string Downloads::GetRawSongData()
	{
		std::string SongID = EncryptedSong.fileID;
		EncryptedSong.rawHostData = HttpRequest("spclient.wg.spotify.com", "/storage-resolve/files/audio/interactive_prefetch/" + SongID + "?product=0", GetAccessToken(), true);
		EncryptedSong.parsedHost = (EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("https://") + 8)).erase(EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("https://") + 8).find("/audio/"));
		EncryptedSong.parsedParam = EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("/audio/")).erase(EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("/audio/")).find("="));
		EncryptedSong.rawSongData = HttpRequest(EncryptedSong.parsedHost, EncryptedSong.parsedParam, "", false);
		return EncryptedSong.rawSongData;
	}

	std::string Downloads::DeleteOGGHeader(std::string rawSongData)
	{
		return rawSongData.substr(rawSongData.find("����OggS") + 4);
	}

	void Downloads::AddTags(std::wstring filename, std::wstring artist, std::wstring album, std::wstring coverarturl)
	{
		wstring newname = L".\\XSongs\\" + filename + L".ogg";
		wstring image = L".\\XSongs\\" + filename + L".jpg";
		URLDownloadToFile(0, coverarturl.c_str(), image.c_str(), 0, 0);

		TagLib::Ogg::Vorbis::File f(newname.c_str());
		TagLib::Ogg::XiphComment* tag = f.tag();
		TagLib::FLAC::Picture* picture = new TagLib::FLAC::Picture();
		ImageFile imageFile(image.c_str());
		TagLib::ByteVector imageData = imageFile.data();

		picture->setData(imageData);
		picture->setType((TagLib::FLAC::Picture::Type)  0x03);
		picture->setMimeType("image/jpeg");
		picture->setDescription("Front Cover");

		TagLib::ByteVector block = picture->render();
		tag->addField("METADATA_BLOCK_PICTURE", Utils::Utils::b64_encode((BYTE*)block.data(), block.size()), true);
		f.tag()->setArtist(artist);
		f.tag()->setAlbum(album);
		f.save();
	}

	void Downloads::DownloadFileProcess()
	{
		std::cout << "[Download started]" << std::endl;

		std::string IVKEY = Utils::Utils::hex2string("72E067FBDDCBCF77EBE8BC643F630D93");
		std::string KEY = Utils::Utils::hex2string(EncryptedSong.decryptionKey);
		int rawSongDataSize = GetRawSongData().size();

		unsigned char* encryptedData = new unsigned char[rawSongDataSize];
		unsigned char* plaintext = new unsigned char[rawSongDataSize];
		memcpy(encryptedData, GetRawSongData().data(), rawSongDataSize);
		DecryptedSong.decryptedtext_len = decrypt(encryptedData, GetRawSongData().length(), (unsigned char*)KEY.c_str(), (unsigned char*)IVKEY.c_str(), plaintext);

		unsigned char* rawDecryptedData = new unsigned char[DecryptedSong.decryptedtext_len];
		DecryptedSong.rawSongData.assign((char*)plaintext, rawSongDataSize);

		unsigned char* outputData = new unsigned char[DeleteOGGHeader(DecryptedSong.rawSongData).size()];
		memcpy(outputData, DeleteOGGHeader(DecryptedSong.rawSongData).data(), DeleteOGGHeader(DecryptedSong.rawSongData).size());

		if (!DecryptedSong.currentSong.empty())
		{
			std::string metadata = HttpRequest("api.spotify.com", "/v1/tracks/" + DecryptedSong.currentSong.substr(DecryptedSong.currentSong.find("spotify:track:") + 14), GetAccessToken(), true);
			//Song name
			DecryptedSong.songName = strtok((char*)(metadata.substr(metadata.find("is_local") + 31)).c_str(), "\"");

			//Artist
			DecryptedSong.artist = strtok((char*)(metadata.substr(metadata.find("name") + 9)).c_str(), "\"");

			//Album
			std::string hmm = Utils::Utils::hex2string("68656967687422203A20363430");
			DecryptedSong.album = strtok((char*)(metadata.substr(metadata.find(hmm) + 404)).c_str(), "\"");

			//Cover
			DecryptedSong.coverart = strtok((char*)(metadata.substr(metadata.find("height") + 30)).c_str(), "\"");

			std::cout << "[Download finished]: " << DecryptedSong.songName << std::endl;
		}

		Utils::Utils::removeForbiddenChar(&DecryptedSong.songName);
		std::ofstream decryptedsongoutput(L".\\XSongs\\" + Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.songName) + L".ogg", std::ios_base::binary);
		decryptedsongoutput.write((char*)outputData, DecryptedSong.decryptedtext_len);
		decryptedsongoutput.close();

		AddTags(Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.songName), Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.artist), Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.album), Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.coverart));
		Utils::Utils::deleteFile(L".\\XSongs\\" + Utils::Utils::ConvertUtf8ToUtf16(DecryptedSong.songName) + L".jpg");

		delete[] encryptedData;
		delete[] plaintext;
		delete[] rawDecryptedData;
		delete[] outputData;
	}

	void __declspec(naked) __fastcall Downloads::Signal_stub(void* _this, DWORD edx, int a2, int a3)
	{
		__asm
		{
			push    ebp
			mov     ebp, esp
			push    -1
			push    117A780h
			push    0B480BAh
			retn
		}
	}

	void __fastcall Downloads::Signal_hk(void* _this, DWORD edx, int a2, int a3)
	{
		Downloads::check = true;
		Signal_stub(_this, edx, a2, a3);
	}

	void __declspec(naked) Downloads::CmdAddText_stub2(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5)
	{
		__asm
		{
			push    ebp
			mov     ebp, esp
			push    esi
			mov     esi, [ebp + 8]
			lea     eax, [ebp + 20]
			push    108BB1Ah
			retn
		}
	}

	void Downloads::CmdAddText_hk2(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5)
	{
		if (fmt[8] == char(116) && fmt[9] == char(114) && fmt[10] == char(97) && fmt[11] == char(99) && fmt[12] == char(107) && fmt[13] == char(95) && fmt[14] == char(117) && fmt[15] == char(114) && fmt[16] == char(105))
		{
			if (dummy0[8] == char(97) && dummy0[9] == char(100))
			{
				HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
				std::cout << "[Ad detected]: " << dummy0 << std::endl;
			}
			else if (dummy0[8] == char(116) && dummy0[9] == char(114))
			{
				DecryptedSong.currentSong = dummy0;
				std::cout << DecryptedSong.currentSong << std::endl;
			}
		}

		CmdAddText_stub2(a1, a2, fmt, dummy0, dummy1, dummy2, dummy3, dummy4, dummy5);
	}

	void __declspec(naked) Downloads::GetFileID_stub(int* a1, int a2)
	{
		__asm
		{
			push    ebp
			mov     ebp, esp
			mov     ecx, [ebp + 8]
			push    dword ptr[ebp + 12]
			push    070B469h
			retn
		}
	}

	void Downloads::CheckUserInputAndStartDownload()
	{
		std::string input;
		std::cout << "Type 'dl' to download the current song!" << std::endl;
		std::getline(std::cin, input);

		if (input == "dl")
		{
			std::thread t1(DownloadFileProcess);
			t1.detach();
		}
	}

	void Downloads::GetFileID_hk(int* a1, int a2)
	{
		if (check)
		{
			EncryptedSong.fileID = (char*)(*(DWORD*)((*(DWORD*)(a1)) + 40));
			std::thread t2(CheckUserInputAndStartDownload);
			t2.detach();
			check = false;
		}

		GetFileID_stub(a1, a2);
	}

	void __fastcall Downloads::SetLinkAvailableOffline_hk(void* __this, DWORD edx, int a2, int* a3, unsigned int* a4, unsigned int a5, int* a6, unsigned __int8 a7)
	{
		std::thread t1(DownloadFileProcess);
		t1.detach();
		SetLinkAvailableOffline_stub(__this, edx, a2, a3, a4, a5, a6, a7);
	}

	void __declspec(naked) __fastcall Downloads::SetLinkAvailableOffline_stub(void* __this, DWORD edx, int a2, int* a3, unsigned int* a4, unsigned int a5, int* a6, unsigned __int8 a7)
	{
		__asm
		{
			push    ebp
			mov     ebp, esp
			push    -1
			push    1169D07h
			push	9FCFDAh
			retn
		}
	}

	Downloads::Downloads()
	{
		Utils::Hook::InstallJmp((void*)0xB480B0, Downloads::Signal_hk);
		Utils::Hook::InstallJmp((void*)0x108BB10, Downloads::CmdAddText_hk2);
		Utils::Hook::InstallJmp((void*)0x106B920, Downloads::AES_set_encrypt_key_hk);
		Utils::Hook::InstallJmp((void*)0x70B460, Downloads::GetFileID_hk);
		Utils::Hook::InstallJmp((void*)0x9FCFD0, Downloads::SetLinkAvailableOffline_hk);
	}
}
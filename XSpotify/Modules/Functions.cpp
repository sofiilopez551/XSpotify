#include "..\\include\BaseInclude.hpp"

namespace Functions
{
	CreateTrack_t CreateTrack = (CreateTrack_t)0xC58980;
	CloseTrack_t CloseTrack = (CloseTrack_t)0xC58130;
	OpenTrack_t OpenTrack = (OpenTrack_t)0xC59E10;
	CmdAddText_t CmdAddText = (CmdAddText_t)0x108BB10;
	SetBitrate_t SetBitrate = (SetBitrate_t)0xE18B80;
	EnableSkips_t EnableSkips = (EnableSkips_t)0x745DB0;
	GetFileID_t GetFileID = (GetFileID_t)0x70B460;
	aes_set_encrypt_key_t aes_set_encrypt_key = (aes_set_encrypt_key_t)0x106B920;
	Signal_t Signal = (Signal_t)0xB480B0;
	SetLinkAvailableOffline_t SetLinkAvailableOffline = (SetLinkAvailableOffline_t)0x9FCFD0;
	ParseUpdateUrl_t ParseUpdateUrl = (ParseUpdateUrl_t)0x56CE1A;
}